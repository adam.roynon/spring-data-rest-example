from kafka import KafkaConsumer
from json import loads

TOPIC="spring-data-rest.location.v1"

consumer = KafkaConsumer(
    TOPIC,
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    group_id='SpringDataRestExample',
    value_deserializer=lambda m: loads(m.decode('utf-8')),
    bootstrap_servers=['localhost:9092'])

for m in consumer:
    print(m.value)