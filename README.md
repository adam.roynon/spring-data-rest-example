# Spring Data Rest Example

A Simple example project to show how Spring Data Rest can be used to quickly spin up a based CRUD service.

Given the Entities and Repository classes, Spring will automatically generate the controllers to create a HATEOAS REST layer.

## Example Get Request Response
```json
{
    "_embedded": {
        "locations": [
            {
                "name": "Port of Jingtang",
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/locations/82fbded8-3c9d-41f8-b651-54ed4f93d31e"
                    },
                    "location": {
                        "href": "http://localhost:8080/locations/82fbded8-3c9d-41f8-b651-54ed4f93d31e"
                    }
                }
            }
        ]
    },
    "_links": {
        "self": {
            "href": "http://localhost:8080/locations"
        },
        "profile": {
            "href": "http://localhost:8080/profile/locations"
        }
    },
    "page": {
        "size": 20,
        "totalElements": 1,
        "totalPages": 1,
        "number": 0
    }
```

## Creating Relationships
To create an entity with relationships you have to include the 'link' value rather than the actual object.

Shown below is an example of a POST request body, where the 'vehicle', 'arrivalPortCall' and 'destinationPortCall' are all relationships to other entities (that already exist).
```json
{
  "vehicle": "/vehicles/bc4e8845-f445-41d2-92a3-84903cf3f818",
  "mode": "OCEAN",
  "arrivalPortCall": "/transportCalls/f929a500-8178-47eb-884d-32481b173f10",
  "destinationPortCall":"/transportCalls/acf1a10d-c3be-4205-991f-ea492d85b2bb"
}
```

## Postman Collection
Within this repo is also a simple Postman collection (*spring_data_rest_example.postman_collection.json*) that contains some simple examples of requests.

## Kafka 
There is a basic setup to demonstrate the ability to publish to a Kafka topic. This Kafka server is a simple unsecured docker container. There is also a small Python Kafka consumer to verify the messages are being sent correctly.

Run the Kafka docker-compose file
```shell
docker-compose -f ./kafka/kafka-docker-compose.yml up
```
Setup and Run the Python Kafka Consumer
```shell
cd kafka
python3 -m vent env
source env/bin/activate
pip install Kafka-python
```
Then run the Spring boot app and send a POST request to the _/location_ endpoint.

## Notes

### DTOs
This service currently doesn't use any DTO's. This can be done via Projections (see example [here](https://github.com/Cepr0/spring-data-rest-dto)). However, if the DTOs and model objects are going to identical (as there are in this example) then there is really no need to have separate DTO classes.

### Extending Repositories
Currently the Repository interfaces are empty, this gives basic requests such as 'findAll' and 'findById'. However, you can extend this to create more complex request types. Such as below. You don't have to provide any implementation/concreate Repository, Spring does it all automagically for you.

```java
public interface VehicleRepository extends PagingAndSortingRepository<Vehicle, UUID> {

    Optional<Vehicle> findByNameAndImoNumber(String name, String imoNumber);
    List<Vehicle> findAllByNameStartsWith(String startsWith);
    List<Vehicle> findAllByNameContainsIgnoreCase(String contains);

}
```

### Change Endpoint Name
Within this service I let Spring automatically choose the endpoint name, which by default is the plural form of the Entity name. Basically this means that the endpoint for the 'Vehicle' entity will be '/vehicles/'. However, you can change this by modifying the @RepositoryRestResource annotation.
````java
@RepositoryRestResource(path = "vehicle")
public interface VehicleRepository extends PagingAndSortingRepository<Vehicle, UUID> {
```