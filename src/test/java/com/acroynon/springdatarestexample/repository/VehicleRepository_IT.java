package com.acroynon.springdatarestexample.repository;

import com.acroynon.springdatarestexample.model.Vehicle;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        // Don't need a whole new app.props file, just override the db url
        properties = {"spring.datasource.url=jdbc:h2:mem:testdb"})
class VehicleRepository_IT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private VehicleRepository repository;

    private static Vehicle existingVehicle = Vehicle.builder()
            .name("Cosco Shipping Star")
            .imoNumber("9795658")
            .build();

    @BeforeEach
    void setup() {
        if (existingVehicle.getId() == null) {
            existingVehicle = repository.save(existingVehicle);
        }
    }

    @Test
    void create_validResult_should201() throws Exception {
        // Given
        Vehicle vehicle = new Vehicle();
        vehicle.setName("MSC Mediterranean");
        vehicle.setImoNumber("9102710");

        // When/Then
        mockMvc.perform(post("/vehicles")
                        .content(objectMapper.writeValueAsString(vehicle))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void getById_existingResult_should202() throws Exception {
        // When/Then
        mockMvc.perform(get("/vehicles/" + existingVehicle.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(existingVehicle.getName()))
                .andExpect(jsonPath("$.imoNumber").value(existingVehicle.getImoNumber()))
                .andExpect(jsonPath("$._links.self.href", containsString(existingVehicle.getId().toString())));
    }

    @Test
    void create_conflictingImoNumber_should409() throws Exception {
        // Given
        Vehicle newVehicle = new Vehicle();
        newVehicle.setImoNumber(existingVehicle.getImoNumber());
        newVehicle.setName("Another Vessel");

        // When/Then
        mockMvc.perform(post("/vehicles")
                        .content(objectMapper.writeValueAsString(newVehicle))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    // Attempting to create an existing/identical entity will return 201
    // It has already been created, so technically it's a 'success'
    @Test
    void create_existingVehicle_should401() throws Exception {
        // When/Then
        mockMvc.perform(post("/vehicles")
                        .content(objectMapper.writeValueAsString(existingVehicle))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }
}
