package com.acroynon.springdatarestexample.repository;

import com.acroynon.springdatarestexample.model.TransportCall;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource
public interface TransportCallRepository extends PagingAndSortingRepository<TransportCall, UUID> {
}
