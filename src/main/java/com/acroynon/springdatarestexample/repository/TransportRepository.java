package com.acroynon.springdatarestexample.repository;

import com.acroynon.springdatarestexample.model.Transport;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource
public interface TransportRepository extends PagingAndSortingRepository<Transport, UUID> {
}
