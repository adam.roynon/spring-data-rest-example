package com.acroynon.springdatarestexample.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = ImoNumberValidator.class)
@Target( { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ImoNumberConstraint {
    String message() default "Invalid IMO Number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
