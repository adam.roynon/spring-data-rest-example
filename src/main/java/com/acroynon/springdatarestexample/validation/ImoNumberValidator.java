package com.acroynon.springdatarestexample.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ImoNumberValidator implements ConstraintValidator<ImoNumberConstraint, String> {
    @Override
    public void initialize(final ImoNumberConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(final String imoNumber, final ConstraintValidatorContext context) {
        return imoNumber != null && imoNumber.matches("\\d{7}");
    }
}
