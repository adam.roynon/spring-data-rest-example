package com.acroynon.springdatarestexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class SpringDataRestExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataRestExampleApplication.class, args);
	}

}
