package com.acroynon.springdatarestexample.aop;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class RepositoryExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity handleConstraintViolation(ConstraintViolationException exception){

        List<ErrorObject> errors = new ArrayList<>();

        exception.getConstraintViolations().stream()
                .forEach(violation -> {
                   errors.add(new ErrorObject(
                           violation.getPropertyPath().toString(),
                           violation.getInvalidValue().toString(),
                           violation.getMessage()));
                });
        log.info("Handling constraint violation with message [{}]", errors);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errors);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity handleDataIntegrityViolation(DataIntegrityViolationException exception){
        Map<String, String> body = new HashMap<>();
        body.put("error", exception.getRootCause().getMessage());
        log.info("Handling data integrity violation with message: [{}]", body.get("error"));
        return ResponseEntity.status(HttpStatus.CONFLICT).body(body);
    }

    @Value
    @AllArgsConstructor
    private class ErrorObject {
        private String property;
        private String invalidValue;
        private String message;
    }

}
