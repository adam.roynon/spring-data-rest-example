package com.acroynon.springdatarestexample.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class BasicKafkaPublisher {

    @Value(value = "${kafka.topics.location}")
    private String topic;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @AfterReturning(value = "execution(* com.acroynon.springdatarestexample.repository.LocationRepository.save(*)) && args(entity)")
    public void afterSavingLocation(Object entity) throws JsonProcessingException {
        log.info("publishing message [{}] to topic", entity, topic);
        kafkaTemplate.send(topic, objectMapper.writeValueAsString(entity));
    }

}
