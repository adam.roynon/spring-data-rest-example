package com.acroynon.springdatarestexample.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.framework.Advised;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class RepositoryGenericAspect {

    @AfterReturning(value = "execution(* com.acroynon.springdatarestexample.repository.*Repository.save(*)) && args(entity)")
    public void afterSavingEntity(Object entity){
        String entityType = entity.getClass().getSimpleName();
        log.info("Saving {} [{}]", entityType, entity);
    }

    @AfterReturning(value = "execution(* com.acroynon.springdatarestexample.repository.*Repository.deleteById(*)) && args(id)")
    public void afterDeletingEntity(JoinPoint joinPoint, Object id){
        for(Class clazz : joinPoint.getTarget().getClass().getInterfaces()){
            String typeName = clazz.getTypeName();
            if(typeName.startsWith("com.acroynon")){
                log.info("Deleting Entity using {} with Id {}", clazz.getSimpleName(), id);
                break;
            }
        }
    }

}
