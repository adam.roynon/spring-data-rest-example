package com.acroynon.springdatarestexample.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransportCall {

    @Id
    // If you're just using UUID this can be removed, but for BeaconId's we will need customer generators
    @GenericGenerator(name = "mygen", strategy = "com.acroynon.springdatarestexample.generator.IdGenerator")
    @GeneratedValue(generator = "mygen")
    @Type(type = "uuid-char")
    private UUID id;

    @OneToOne
    private Location location;

}
